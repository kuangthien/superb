<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

// Update theme package values

// Update theme package values
$configUpdate = new Mage_Core_Model_Config();
$configUpdate->saveConfig('design/package/name','rwd', 'default', 0);
$configUpdate->saveConfig('design/theme/template','smart', 'default', 0);
$configUpdate->saveConfig('design/theme/skin','smart', 'default', 0);
$configUpdate->saveConfig('design/theme/layout','smart', 'default', 0);


$installer->endSetup();